// Rename the LibAnswers form submit button
jQuery(document).ready(function ($) {
  // See https://swizec.com/blog/
  function waitForForm() {
    if (!$('.s-la-searchform-button').html()) {
      window.requestAnimationFrame(waitForForm);
    }
    $('.s-la-searchform-button').html('Submit question');
  }
  waitForForm();
});
