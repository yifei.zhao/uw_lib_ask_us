Library Ask Us Module
---------------------

THIS REPOSITORY IS REPLACED BY THE VERSION THAT USES DRUPAL FEATURES AND 
CONTEXTS. PLEASE USE https://git.uwaterloo.ca/library/uw_lib_ask_us AS THE 
SOURCE CODE FOR THE 'ASK US' MODULE.



This module introduces some JavaScript required for the LibAnswers and LibChat products
used by the UW Library.

It creates two custom blocks on the /services/ask-us page of the Library's WCMS site.

1. LibAnswers search form in the main content
2. LibChat chatbox within an <iframe> and some Skype links, in sidebar second 

To activate the module:
- Ensure there is a /services/ask-us page created on the respective site
- Clone the repo into the respective /modules folder on the site
- Enable the module in the Admin
- Move the LibAnswers form block to the top of the main content, if required
- Move the LibChat/Skype block to the top of sidebar second, if required

The source code is available at https://git.uwaterloo.ca/yifei.zhao/uw_lib_ask_us.

The module is maintained by:
- Graham Faulkner <g2faulkn@uwaterloo.ca>
- Yifei Zhao <yifei.zhao@uwaterloo.ca>
